﻿using Restaurant.DomainAbstractions.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurant.Entities.Code
{
    [Table("Positions")]
    public class Position: DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        public string NameOfPosition { get; set; }
    }
}
