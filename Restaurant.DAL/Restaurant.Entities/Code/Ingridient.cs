﻿using Restaurant.DomainAbstractions.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurant.Entities.Code
{
    [Table("Ingridients")]
    public class Ingridient: DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        public Product Product { get; set; }
        public double Count { get; set; }
    }
}
