﻿using Restaurant.DomainAbstractions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurant.Entities.Code
{
    [Table("Products")]
    public class Product : DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual List<CounterName> CounterNames { get; set; }
    }
}
