﻿using Restaurant.DomainAbstractions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurant.Entities.Code
{
    [Table("Sales")]
    public class Sale: DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        public virtual List<Dish> Dishes { get; set; }
        public virtual List<Employee> Oficiant { get; set; }
    }
}
