﻿using Restaurant.DomainAbstractions.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restaurant.Entities.Code
{
    [Table("Purchases")]
    public class Purchase: DbEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        new public Guid Id { get; set; }
        public Seller Seller { get; set; }
        public virtual List<Product> Products { get; set; }
        public List<double> Count { get; set; }
    }
}
