﻿using Restaurant.AppContext.Code;
using Restaurant.Entities.Code;
using Restaurant.Repositories.Abstract;
using Restaurant.Repositories.Generic;

namespace Restaurant.Repositories
{
    public class CounterNamesRepository : DbRepository<CounterName>, ICounterNamesRepository
    {
        public CounterNamesRepository(MyAppDbContext context)
            : base(context)
        {
            
        }
    }

    public class DishesRepository : DbRepository<Dish>, IDishesRepository
    {
        public DishesRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class EmployeesRepository : DbRepository<Employee>, IEmployeesRepository
    {
        public EmployeesRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class IngridientsRepository : DbRepository<Ingridient>, IIngridientsRepository
    {
        public IngridientsRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class PositionsRepository : DbRepository<Position>, IPositionsRepository
    {
        public PositionsRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class ProductsRepository : DbRepository<Product>, IProductsRepository
    {
        public ProductsRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class PurchasesRepository : DbRepository<Purchase>, IPurchasesRepository
    {
        public PurchasesRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class RecipesRepository : DbRepository<Recipe>, IRecipesRepository
    {
        public RecipesRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class SalesRepository : DbRepository<Sale>, ISalesRepository
    {
        public SalesRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class SellersRepository : DbRepository<Seller>, ISellersRepository
    {
        public SellersRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }

    public class StocksRepository : DbRepository<Stock>, IStocksRepository
    {
        public StocksRepository(MyAppDbContext context)
            : base(context)
        {
        }
    }
}
