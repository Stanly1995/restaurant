﻿using Restaurant.DomainAbstractions.Repositories;
using Restaurant.Entities.Code;

namespace Restaurant.Repositories.Abstract
{

    public interface ICounterNamesRepository : IDbRepository<CounterName>
    {
    }

    public interface IDishesRepository : IDbRepository<Dish>
    {
    }

    public interface IEmployeesRepository : IDbRepository<Employee>
    {
    }

    public interface IIngridientsRepository : IDbRepository<Ingridient>
    {
    }

    public interface IPositionsRepository : IDbRepository<Position>
    {
    }

    public interface IProductsRepository : IDbRepository<Product>
    {
    }

    public interface IPurchasesRepository : IDbRepository<Purchase>
    {
    }

    public interface IRecipesRepository : IDbRepository<Recipe>
    {
    }

    public interface ISalesRepository : IDbRepository<Sale>
    {
    }

    public interface ISellersRepository : IDbRepository<Seller>
    {
    }

    public interface IStocksRepository : IDbRepository<Stock>
    {
    }
}
