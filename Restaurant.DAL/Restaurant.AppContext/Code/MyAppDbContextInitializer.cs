﻿using System.Data.Entity;

namespace Restaurant.AppContext.Code
{
    class MyAppDbContextInitializer : DropCreateDatabaseIfModelChanges<MyAppDbContext>
    {
        protected override void Seed(MyAppDbContext context)
        {

        }
    }
}
