﻿using Restaurant.AppContext.Code;
using Restaurant.Repositories;
using Restaurant.Repositories.Abstract;

namespace Restaurant.DataUnit
{
    public static class Unit
    {
        static MyAppDbContext _context;
        public static ICounterNamesRepository CounterNamesRepository { get; private set; }
        public static IDishesRepository DishesRepository { get; private set; }
        public static IEmployeesRepository EmployeesRepository { get; private set; }
        public static IIngridientsRepository IngridientsRepository { get; private set; }
        public static IPositionsRepository PositionsRepository { get; private set; }
        public static IProductsRepository ProductsRepository { get; private set; }
        public static IPurchasesRepository PurchasesRepository { get; private set; }
        public static IRecipesRepository RecipesRepository { get; private set; }
        public static ISalesRepository SalesRepository { get; private set; }
        public static ISellersRepository SellersRepository { get; private set; }
        public static IStocksRepository StocksRepository { get; private set; }

       static Unit()
        {
            _context = new MyAppDbContext("RestaurantDB");
            CounterNamesRepository = new CounterNamesRepository(_context);
            DishesRepository = new DishesRepository(_context);
            EmployeesRepository = new EmployeesRepository(_context);
            IngridientsRepository = new IngridientsRepository(_context);
            PositionsRepository = new PositionsRepository(_context);
            ProductsRepository = new ProductsRepository(_context);
            PurchasesRepository = new PurchasesRepository(_context);
            RecipesRepository = new RecipesRepository(_context);
            SalesRepository = new SalesRepository(_context);
            SellersRepository = new SellersRepository(_context);
            StocksRepository = new StocksRepository(_context);
        }
    }
}
