﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Restaurant.DomainAbstractions.Entities
{
    public interface IDbEntity
    {
        [Key]
        Guid Id { get; set; }
    }
}
