﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.DataUnit;
using System.Linq;
using System.Collections.Generic;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Services;

namespace Restaurant.UnitTests
{
    [TestClass]
    public class DishTest
    {
        DishService service = new DishService();

        [TestMethod]
        public void AddItemTest1()
        {
            var item = new DishDTO() { Name = "салат" };
            Assert.AreEqual(true, service.AddItem(item));
        }

        [TestMethod]
        public void GetItemTest1()
        {
            var item = Unit.DishesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("салат") == 0);
            Assert.AreEqual("салат", service.GetItem(item.Id).Name);
        }

        [TestMethod]
        public void SaveChangesTest1()
        {
            Assert.AreEqual(true, service.SaveChanges());
        }

        [TestMethod]
        public void DeleteItemTest1()
        {
            var item = Unit.DishesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("салат") == 0);
            Assert.AreEqual(true, service.DeleteItem(item.Id));
            service.SaveChanges();
            item = Unit.DishesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("салат") == 0);
            Assert.AreEqual(null, item);
        }

        [TestMethod]
        public void AddItemsTest1()
        {
            var item1 = new DishDTO() { Name = "борщ" };
            var item2 = new DishDTO() { Name = "вареники" };
            var list = new List<DishDTO>() { item1, item2 };
            Assert.AreEqual(true, service.AddItems(list));
            service.SaveChanges();
            var dto1 = Unit.DishesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("борщ") == 0);
            var dto2 = Unit.DishesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("вареники") == 0);
            Assert.AreNotEqual(null, dto1);
            Assert.AreNotEqual(null, dto2);
        }
    }
}
