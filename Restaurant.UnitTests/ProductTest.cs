﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.DataUnit;
using System.Linq;
using System.Collections.Generic;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Services;

namespace Restaurant.UnitTests
{
    [TestClass]
    public class ProductTest
    {
        ProductService service = new ProductService();

        [TestMethod]
        public void AddItemTest1()
        {
            var item = new ProductDTO() { Name = "Картофель" };
            Assert.AreEqual(true, service.AddItem(item));
        }

        [TestMethod]
        public void GetItemTest1()
        {
            var item = Unit.ProductsRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Картофель") == 0);
            Assert.AreEqual("Картофель", service.GetItem(item.Id).Name);
        }

        [TestMethod]
        public void SaveChangesTest1()
        {
            Assert.AreEqual(true, service.SaveChanges());
        }

        [TestMethod]
        public void DeleteItemTest1()
        {
            var item = Unit.ProductsRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Картофель") == 0);
            Assert.AreEqual(true, service.DeleteItem(item.Id));
            service.SaveChanges();
            item = Unit.ProductsRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Картофель") == 0);
            Assert.AreEqual(null, item);
        }

        [TestMethod]
        public void AddItemsTest1()
        {
            var item1 = new ProductDTO() { Name = "Свекла" };
            var item2 = new ProductDTO() { Name = "Хлеб" };
            var list = new List<ProductDTO>() { item1, item2 };
            Assert.AreEqual(true, service.AddItems(list));
            service.SaveChanges();
            var dto1 = Unit.ProductsRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Свекла") == 0);
            var dto2 = Unit.ProductsRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Хлеб") == 0);
            Assert.AreNotEqual(null, dto1);
            Assert.AreNotEqual(null, dto2);
        }
    }
}
