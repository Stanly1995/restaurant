﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.DataUnit;
using System.Linq;
using System.Collections.Generic;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Services;

namespace Restaurant.UnitTests
{
    [TestClass]
    public class IngridientTest
    {
        IngridientService service = new IngridientService();

        [TestMethod]
        public void AddItemTest1()
        {
            var item = new IngridientDTO() { Count = 3  };
            Assert.AreEqual(true, service.AddItem(item));
        }

        [TestMethod]
        public void GetItemTest1()
        {
            var item = Unit.IngridientsRepository.AllItems.FirstOrDefault(x => x.Count.CompareTo(3) == 0);
            Assert.AreEqual(3, service.GetItem(item.Id).Count);
        }

        [TestMethod]
        public void SaveChangesTest1()
        {
            Assert.AreEqual(true, service.SaveChanges());
        }

        [TestMethod]
        public void DeleteItemTest1()
        {
            var item = Unit.IngridientsRepository.AllItems.FirstOrDefault(x => x.Count.CompareTo(3) == 0);
            Assert.AreEqual(true, service.DeleteItem(item.Id));
            service.SaveChanges();
            item = Unit.IngridientsRepository.AllItems.FirstOrDefault(x => x.Count.CompareTo(3) == 0);
            Assert.AreEqual(null, item);
        }

        [TestMethod]
        public void AddItemsTest1()
        {
            var item1 = new IngridientDTO() { Count = 4 };
            var item2 = new IngridientDTO() { Count = 5 };
            var list = new List<IngridientDTO>() { item1, item2 };
            Assert.AreEqual(true, service.AddItems(list));
            service.SaveChanges();
            var dto1 = Unit.IngridientsRepository.AllItems.FirstOrDefault(x => x.Count.CompareTo(4) == 0);
            var dto2 = Unit.IngridientsRepository.AllItems.FirstOrDefault(x => x.Count.CompareTo(5) == 0);
            Assert.AreNotEqual(null, dto1);
            Assert.AreNotEqual(null, dto2);
        }
    }
}
