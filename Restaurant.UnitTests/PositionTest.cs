﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.DataUnit;
using System.Linq;
using System.Collections.Generic;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Services;

namespace Restaurant.UnitTests
{
    [TestClass]
    public class PositionTest
    {
        PositionService service = new PositionService();

        [TestMethod]
        public void AddItemTest1()
        {
            var item = new PositionDTO() { NameOfPosition = "Официант" };
            Assert.AreEqual(true, service.AddItem(item));
        }

        [TestMethod]
        public void GetItemTest1()
        {
            var item = Unit.PositionsRepository.AllItems.FirstOrDefault(x => x.NameOfPosition.CompareTo("Официант") == 0);
            Assert.AreEqual("Официант", service.GetItem(item.Id).NameOfPosition);
        }

        [TestMethod]
        public void SaveChangesTest1()
        {
            Assert.AreEqual(true, service.SaveChanges());
        }

        [TestMethod]
        public void DeleteItemTest1()
        {
            var item = Unit.PositionsRepository.AllItems.FirstOrDefault(x => x.NameOfPosition.CompareTo("Официант") == 0);
            Assert.AreEqual(true, service.DeleteItem(item.Id));
            service.SaveChanges();
            item = Unit.PositionsRepository.AllItems.FirstOrDefault(x => x.NameOfPosition.CompareTo("Официант") == 0);
            Assert.AreEqual(null, item);
        }

        [TestMethod]
        public void AddItemsTest1()
        {
            var item1 = new PositionDTO() { NameOfPosition = "Повар" };
            var item2 = new PositionDTO() { NameOfPosition = "Администратор" };
            var list = new List<PositionDTO>() { item1, item2 };
            Assert.AreEqual(true, service.AddItems(list));
            service.SaveChanges();
            var dto1 = Unit.PositionsRepository.AllItems.FirstOrDefault(x => x.NameOfPosition.CompareTo("Повар") == 0);
            var dto2 = Unit.PositionsRepository.AllItems.FirstOrDefault(x => x.NameOfPosition.CompareTo("Администратор") == 0);
            Assert.AreNotEqual(null, dto1);
            Assert.AreNotEqual(null, dto2);
        }
    }
}
