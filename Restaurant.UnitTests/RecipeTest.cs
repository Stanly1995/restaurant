﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.DataUnit;
using System.Linq;
using System.Collections.Generic;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Services;

namespace Restaurant.UnitTests
{
    [TestClass]
    public class RecipeTest
    {
        RecipeService service = new RecipeService();

        [TestMethod]
        public void AddItemTest1()
        {
            var item = new RecipeDTO() { TextOfRecipe = "Жарить" };
            Assert.AreEqual(true, service.AddItem(item));
        }

        [TestMethod]
        public void GetItemTest1()
        {
            var item = Unit.RecipesRepository.AllItems.FirstOrDefault(x => x.TextOfRecipe.CompareTo("Жарить") == 0);
            Assert.AreEqual("Жарить", service.GetItem(item.Id).TextOfRecipe);
        }

        [TestMethod]
        public void SaveChangesTest1()
        {
            Assert.AreEqual(true, service.SaveChanges());
        }

        [TestMethod]
        public void DeleteItemTest1()
        {
            var item = Unit.RecipesRepository.AllItems.FirstOrDefault(x => x.TextOfRecipe.CompareTo("Жарить") == 0);
            Assert.AreEqual(true, service.DeleteItem(item.Id));
            service.SaveChanges();
            item = Unit.RecipesRepository.AllItems.FirstOrDefault(x => x.TextOfRecipe.CompareTo("Жарить") == 0);
            Assert.AreEqual(null, item);
        }

        [TestMethod]
        public void AddItemsTest1()
        {
            var item1 = new RecipeDTO() { TextOfRecipe = "варить" };
            var item2 = new RecipeDTO() { TextOfRecipe = "парить" };
            var list = new List<RecipeDTO>() { item1, item2 };
            Assert.AreEqual(true, service.AddItems(list));
            service.SaveChanges();
            var dto1 = Unit.RecipesRepository.AllItems.FirstOrDefault(x => x.TextOfRecipe.CompareTo("варить") == 0);
            var dto2 = Unit.RecipesRepository.AllItems.FirstOrDefault(x => x.TextOfRecipe.CompareTo("парить") == 0);
            Assert.AreNotEqual(null, dto1);
            Assert.AreNotEqual(null, dto2);
        }
    }
}
