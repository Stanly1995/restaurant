﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.DataUnit;
using System.Linq;
using System.Collections.Generic;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Services;

namespace Restaurant.UnitTests
{
    [TestClass]
    public class SellerTest
    {
        SellerService service = new SellerService();

        [TestMethod]
        public void AddItemTest1()
        {
            var item = new SellerDTO() { Name = "Продавец1" };
            Assert.AreEqual(true, service.AddItem(item));
        }

        [TestMethod]
        public void GetItemTest1()
        {
            var item = Unit.SellersRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Продавец1") == 0);
            Assert.AreEqual("Продавец1", service.GetItem(item.Id).Name);
        }

        [TestMethod]
        public void SaveChangesTest1()
        {
            Assert.AreEqual(true, service.SaveChanges());
        }

        [TestMethod]
        public void DeleteItemTest1()
        {
            var item = Unit.SellersRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Продавец1") == 0);
            Assert.AreEqual(true, service.DeleteItem(item.Id));
            service.SaveChanges();
            item = Unit.SellersRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Продавец1") == 0);
            Assert.AreEqual(null, item);
        }

        [TestMethod]
        public void AddItemsTest1()
        {
            var item1 = new SellerDTO() { Name = "Продавец2" };
            var item2 = new SellerDTO() { Name = "Продавец3" };
            var list = new List<SellerDTO>() { item1, item2 };
            Assert.AreEqual(true, service.AddItems(list));
            service.SaveChanges();
            var dto1 = Unit.SellersRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Продавец2") == 0);
            var dto2 = Unit.SellersRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("Продавец3") == 0);
            Assert.AreNotEqual(null, dto1);
            Assert.AreNotEqual(null, dto2);
        }
    }
}
