﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.DataUnit;
using System.Linq;
using System.Collections.Generic;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Services;

namespace Restaurant.UnitTests
{
    [TestClass]
    public class EmployeeTest
    {
        EmployeeService service = new EmployeeService();

        [TestMethod]
        public void AddItemTest1()
        {
            var item = new EmployeeDTO() { FirstName = "Саша" };
            Assert.AreEqual(true, service.AddItem(item));
        }

        [TestMethod]
        public void GetItemTest1()
        {
            var item = Unit.EmployeesRepository.AllItems.FirstOrDefault(x => x.FirstName.CompareTo("Саша") == 0);
            Assert.AreEqual("Саша", service.GetItem(item.Id).FirstName);
        }

        [TestMethod]
        public void SaveChangesTest1()
        {
            Assert.AreEqual(true, service.SaveChanges());
        }

        [TestMethod]
        public void DeleteItemTest1()
        {
            var item = Unit.EmployeesRepository.AllItems.FirstOrDefault(x => x.FirstName.CompareTo("Саша") == 0);
            Assert.AreEqual(true, service.DeleteItem(item.Id));
            service.SaveChanges();
            item = Unit.EmployeesRepository.AllItems.FirstOrDefault(x => x.FirstName.CompareTo("Саша") == 0);
            Assert.AreEqual(null, item);
        }

        [TestMethod]
        public void AddItemsTest1()
        {
            var item1 = new EmployeeDTO() { FirstName = "Петя" };
            var item2 = new EmployeeDTO() { FirstName = "Боря" };
            var list = new List<EmployeeDTO>() { item1, item2 };
            Assert.AreEqual(true, service.AddItems(list));
            service.SaveChanges();
            var dto1 = Unit.EmployeesRepository.AllItems.FirstOrDefault(x => x.FirstName.CompareTo("Петя") == 0);
            var dto2 = Unit.EmployeesRepository.AllItems.FirstOrDefault(x => x.FirstName.CompareTo("Боря") == 0);
            Assert.AreNotEqual(null, dto1);
            Assert.AreNotEqual(null, dto2);
        }
    }
}
