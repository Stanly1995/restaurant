﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Restaurant.DataUnit;
using System.Linq;
using System.Collections.Generic;
using Restaurant.BLL.DTO;
using Restaurant.BLL.Services;

namespace Restaurant.UnitTests
{
    [TestClass]
    public class CounterNameTest
    {
        CounterNameService service = new CounterNameService();

        [TestMethod]
        public void AddItemTest1()
        {
            var item = new CounterNameDTO() { Name = "грамм" };
            Assert.AreEqual(true, service.AddItem(item));
        }

        [TestMethod]
        public void GetItemTest1()
        {
            var item = Unit.CounterNamesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("грамм") == 0);
            Assert.AreEqual("грамм", service.GetItem(item.Id).Name);
        }

        [TestMethod]
        public void SaveChangesTest1()
        {
            Assert.AreEqual(true, service.SaveChanges());
        }

        [TestMethod]
        public void DeleteItemTest1()
        {
            var item = Unit.CounterNamesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("грамм") == 0);
            Assert.AreEqual(true, service.DeleteItem(item.Id));
            service.SaveChanges();
            item = Unit.CounterNamesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("грамм") == 0);
            Assert.AreEqual(null, item);
        }

        [TestMethod]
        public void AddItemsTest1()
        {
            var item1 = new CounterNameDTO() { Name = "литр" };
            var item2 = new CounterNameDTO() { Name = "килограмм" };
            var list = new List<CounterNameDTO>() { item1, item2 };
            Assert.AreEqual(true, service.AddItems(list));
            service.SaveChanges();
            var dto1 = Unit.CounterNamesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("литр") == 0);
            var dto2 = Unit.CounterNamesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("килограмм") == 0);
            Assert.AreNotEqual(null, dto1);
            Assert.AreNotEqual(null, dto2);
        }

        //[TestMethod]
        public void ChangeItemTest1()
        {
            var item1 = new CounterNameDTO() { Name = "милилитр" };
            service.AddItem(item1);
            var item = Unit.CounterNamesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("милилитр") == 0);
            var item2 = service.GetItem(item.Id);
            item2.Name = "центнер";
            service.ChangeItem(item2);
            Assert.AreEqual(null, Unit.CounterNamesRepository.AllItems.FirstOrDefault(x => x.Name.CompareTo("милилитр") == 0));

        }
    }
}
