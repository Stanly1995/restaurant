﻿using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.DTO
{
    public class DishDTO : DTOItem
    {
        public string Name { get; set; }
        public List<Ingridient> Ingridients { get; set; }
        public Recipe Recipe { get; set; }
    }
}
