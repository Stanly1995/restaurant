﻿using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.DTO
{
    public class CounterNameDTO: DTOItem
    {
        public string Name { get; set; }
        public List<Product> ProductsCounter { get; set; }
    }
}
