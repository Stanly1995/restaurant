﻿using Restaurant.BLL.Abstractions;

namespace Restaurant.BLL.DTO
{
    public class PositionDTO : DTOItem
    {
        public string NameOfPosition { get; set; }
    }
}
