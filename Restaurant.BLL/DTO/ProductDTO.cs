﻿using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.DTO
{
    public class ProductDTO : DTOItem
    {
        public string Name { get; set; }
        public List<CounterName> CounterNames { get; set; }
    }
}
