﻿using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.DTO
{
    public class StockDTO : DTOItem
    {
        public List<Product> Product { get; set; }
        public double Count { get; set; }
    }
}
