﻿using Restaurant.BLL.Abstractions;

namespace Restaurant.BLL.DTO
{
    public class RecipeDTO : DTOItem
    {
        public string TextOfRecipe { get; set; }
    }
}
