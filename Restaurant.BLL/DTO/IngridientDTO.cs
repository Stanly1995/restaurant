﻿using Restaurant.BLL.Abstractions;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.DTO
{
    public class IngridientDTO : DTOItem
    {
        public Product Product { get; set; }
        public double Count { get; set; }
    }
}
