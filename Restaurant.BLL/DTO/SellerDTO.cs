﻿using Restaurant.BLL.Abstractions;

namespace Restaurant.BLL.DTO
{
    public class SellerDTO : DTOItem
    {
        public string Name { get; set; }
    }
}
