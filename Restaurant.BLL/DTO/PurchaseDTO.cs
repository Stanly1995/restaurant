﻿using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.DTO
{
    public class PurchaseDTO : DTOItem
    {
        public Seller Seller { get; set; }
        public List<Product> Products { get; set; }
        public List<double> Count { get; set; }
    }
}
