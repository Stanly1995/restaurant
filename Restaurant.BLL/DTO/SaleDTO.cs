﻿using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.DTO
{
    public class SaleDTO : DTOItem
    {
        public List<Dish> Dishes { get; set; }
        public List<Employee> Oficiant { get; set; }
    }
}
