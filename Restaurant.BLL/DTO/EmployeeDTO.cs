﻿using Restaurant.BLL.Abstractions;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.DTO
{
    public class EmployeeDTO : DTOItem
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int Phone { get; set; }
        public string Passport { get; set; }
        public virtual Position Position { get; set; }
    }
}
