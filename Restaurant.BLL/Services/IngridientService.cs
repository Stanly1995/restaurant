﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class IngridientService : DTOService<IngridientDTO>
    {
        public override bool AddItem(IngridientDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.IngridientsRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<IngridientDTO> DTOs)
        {
            List<Ingridient> list = new List<Ingridient>();
            foreach (IngridientDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.IngridientsRepository.AddItems(list);
        }

        public override bool ChangeItem(IngridientDTO DTO)
        {
            return Unit.IngridientsRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.IngridientsRepository.DeleteItem(id);
        }

        public override IngridientDTO GetItem(Guid id)
        {
            Ingridient item = Unit.IngridientsRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.IngridientsRepository.SaveChanges();
        }

        public Ingridient DTOToEntity(IngridientDTO DTO)
        {
            var item = new Ingridient() { Product = DTO.Product, Count = DTO.Count};
            return item;
        }

        public IngridientDTO EntityToDTO(Ingridient Entity)
        {
            var item = new IngridientDTO() { Product = Entity.Product, Count = Entity.Count };
            return item;
        }
    }
}
