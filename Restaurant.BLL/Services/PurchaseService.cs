﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class PurchaseService : DTOService<PurchaseDTO>
    {
        public override bool AddItem(PurchaseDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.PurchasesRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<PurchaseDTO> DTOs)
        {
            List<Purchase> list = new List<Purchase>();
            foreach (PurchaseDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.PurchasesRepository.AddItems(list);
        }

        public override bool ChangeItem(PurchaseDTO DTO)
        {
            return Unit.PurchasesRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.PurchasesRepository.DeleteItem(id);
        }

        public override PurchaseDTO GetItem(Guid id)
        {
            Purchase item = Unit.PurchasesRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.PurchasesRepository.SaveChanges();

        }

        public Purchase DTOToEntity(PurchaseDTO DTO)
        {
            var item = new Purchase() { Products = DTO.Products, Seller = DTO.Seller, Count = DTO.Count };
            return item;
        }

        public PurchaseDTO EntityToDTO(Purchase Entity)
        {
            var item = new PurchaseDTO() { Products = Entity.Products, Seller = Entity.Seller, Count = Entity.Count};
            return item;
        }
    }
}
