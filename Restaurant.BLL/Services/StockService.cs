﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class StockService : DTOService<StockDTO>
    {
        public override bool AddItem(StockDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.StocksRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<StockDTO> DTOs)
        {
            List<Stock> list = new List<Stock>();
            foreach (StockDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.StocksRepository.AddItems(list);
        }

        public override bool ChangeItem(StockDTO DTO)
        {
            return Unit.StocksRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.StocksRepository.DeleteItem(id);
        }

        public override StockDTO GetItem(Guid id)
        {
            Stock item = Unit.StocksRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.StocksRepository.SaveChanges();
        }

        public Stock DTOToEntity(StockDTO DTO)
        {
            var item = new Stock() { Product = DTO.Product, Count = DTO.Count };
            return item;
        }

        public StockDTO EntityToDTO(Stock Entity)
        {
            var item = new StockDTO() { Product = Entity.Product, Count = Entity.Count };
            return item;
        }
    }
}
