﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class SellerService : DTOService<SellerDTO>
    {
        public override bool AddItem(SellerDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.SellersRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<SellerDTO> DTOs)
        {
            List<Seller> list = new List<Seller>();
            foreach (SellerDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.SellersRepository.AddItems(list);
        }

        public override bool ChangeItem(SellerDTO DTO)
        {
            return Unit.SellersRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.SellersRepository.DeleteItem(id);
        }

        public override SellerDTO GetItem(Guid id)
        {
            Seller item = Unit.SellersRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.SellersRepository.SaveChanges();
        }

        public Seller DTOToEntity(SellerDTO DTO)
        {
            var item = new Seller() { Name = DTO.Name };
            return item;
        }

        public SellerDTO EntityToDTO(Seller Entity)
        {
            var item = new SellerDTO() { Name = Entity.Name };
            return item;
        }
    }
}
