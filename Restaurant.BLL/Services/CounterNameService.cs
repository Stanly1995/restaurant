﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class CounterNameService : DTOService<CounterNameDTO>
    {

        public override bool AddItem(CounterNameDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.CounterNamesRepository.AddItem(item);
            
        }

        public override bool AddItems(IEnumerable<CounterNameDTO> DTOs)
        {
            List<CounterName> list = new List<CounterName>();
            foreach (CounterNameDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.CounterNamesRepository.AddItems(list);
        }

        private CounterName DTOToEntity(CounterNameDTO DTO)
        {
            var item = new CounterName() { Name = DTO.Name, ProductsCounter = DTO.ProductsCounter };
            return item;
        }

        private CounterNameDTO EntityToDTO(CounterName Entity)
        {
            var item = new CounterNameDTO() { Name = Entity.Name, ProductsCounter = Entity.ProductsCounter };
            return item;
        }

        public override bool ChangeItem(CounterNameDTO DTO)
        {
            return Unit.CounterNamesRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.CounterNamesRepository.DeleteItem(id);
        }

        public override CounterNameDTO GetItem(Guid id)
        {
            CounterName item = Unit.CounterNamesRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.CounterNamesRepository.SaveChanges();
        }
    }
}
