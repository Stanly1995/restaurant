﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class DishService : DTOService<DishDTO>
    {

        public override bool AddItem(DishDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.DishesRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<DishDTO> DTOs)
        {
            List<Dish> list = new List<Dish>();
            foreach (DishDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.DishesRepository.AddItems(list);
        }

        public override bool ChangeItem(DishDTO DTO)
        {
            return Unit.DishesRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.DishesRepository.DeleteItem(id);
        }

        private Dish DTOToEntity(DishDTO DTO)
        {
            var item = new Dish() { Name = DTO.Name, Ingridients = DTO.Ingridients, Recipe = DTO.Recipe };
            return item;
        }

        private DishDTO EntityToDTO(Dish Entity)
        {
            var item = new DishDTO() { Name = Entity.Name, Ingridients = Entity.Ingridients, Recipe = Entity.Recipe };
            return item;
        }

        public override DishDTO GetItem(Guid id)
        {
            Dish item = Unit.DishesRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.DishesRepository.SaveChanges();
        }
    }
}
