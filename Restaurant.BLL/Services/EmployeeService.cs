﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class EmployeeService : DTOService<EmployeeDTO>
    {
        public override bool AddItem(EmployeeDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.EmployeesRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<EmployeeDTO> DTOs)
        {
            List<Employee> list = new List<Employee>();
            foreach (EmployeeDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.EmployeesRepository.AddItems(list);
        }

        public override bool ChangeItem(EmployeeDTO DTO)
        {
            return Unit.EmployeesRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.EmployeesRepository.DeleteItem(id);
        }

        public override EmployeeDTO GetItem(Guid id)
        {
            Employee item = Unit.EmployeesRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.CounterNamesRepository.SaveChanges();
        }

        public Employee DTOToEntity(EmployeeDTO DTO)
        {
            var item = new Employee() { FirstName = DTO.FirstName, LastName = DTO.LastName, Passport = DTO.Passport, Phone = DTO.Phone, Position = DTO.Position};
            return item;
        }

        public EmployeeDTO EntityToDTO(Employee Entity)
        {
            var item = new EmployeeDTO() { FirstName = Entity.FirstName, LastName = Entity.LastName, Passport = Entity.Passport, Phone = Entity.Phone, Position = Entity.Position };
            return item;
        }
    }
}
