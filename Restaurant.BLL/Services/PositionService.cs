﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class PositionService : DTOService<PositionDTO>
    {
        public override bool AddItem(PositionDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.PositionsRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<PositionDTO> DTOs)
        {
            List<Position> list = new List<Position>();
            foreach (PositionDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.PositionsRepository.AddItems(list);
        }

        public override bool ChangeItem(PositionDTO DTO)
        {
            return Unit.PositionsRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.PositionsRepository.DeleteItem(id);
        }

        public override PositionDTO GetItem(Guid id)
        {
            Position item = Unit.PositionsRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.PositionsRepository.SaveChanges();
        }

        public Position DTOToEntity(PositionDTO DTO)
        {
            var item = new Position() { NameOfPosition = DTO.NameOfPosition };
            return item;
        }

        public PositionDTO EntityToDTO(Position Entity)
        {
            var item = new PositionDTO() { NameOfPosition = Entity.NameOfPosition };
            return item;
        }
    }
}
