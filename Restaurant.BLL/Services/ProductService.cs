﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class ProductService : DTOService<ProductDTO>
    {
        public override bool AddItem(ProductDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.ProductsRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<ProductDTO> DTOs)
        {
            List<Product> list = new List<Product>();
            foreach (ProductDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.ProductsRepository.AddItems(list);
        }

        public override bool ChangeItem(ProductDTO DTO)
        {
            return Unit.ProductsRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.ProductsRepository.DeleteItem(id);

        }

        public override ProductDTO GetItem(Guid id)
        {
            Product item = Unit.ProductsRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.ProductsRepository.SaveChanges();
        }

        public Product DTOToEntity(ProductDTO DTO)
        {
            var item = new Product() { Name = DTO.Name, CounterNames = DTO.CounterNames };
            return item;
        }

        public ProductDTO EntityToDTO(Product Entity)
        {
            var item = new ProductDTO() { Name = Entity.Name, CounterNames = Entity.CounterNames };
            return item;
        }
    }
}
