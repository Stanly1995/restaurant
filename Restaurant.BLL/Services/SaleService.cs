﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class SaleService : DTOService<SaleDTO>
    {
        public override bool AddItem(SaleDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.SalesRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<SaleDTO> DTOs)
        {
            List<Sale> list = new List<Sale>();
            foreach (SaleDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.SalesRepository.AddItems(list);
        }

        public override bool ChangeItem(SaleDTO DTO)
        {
            return Unit.SalesRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.SalesRepository.DeleteItem(id);
        }

        public override SaleDTO GetItem(Guid id)
        {
            Sale item = Unit.SalesRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.SalesRepository.SaveChanges();
        }

        public Sale DTOToEntity(SaleDTO DTO)
        {
            var item = new Sale() { Dishes = DTO.Dishes, Oficiant = DTO.Oficiant };
            return item;
        }

        public SaleDTO EntityToDTO(Sale Entity)
        {
            var item = new SaleDTO() { Dishes = Entity.Dishes, Oficiant = Entity.Oficiant };
            return item;
        }
    }
}
