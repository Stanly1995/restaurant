﻿using System;
using System.Collections.Generic;
using Restaurant.BLL.Abstractions;
using Restaurant.BLL.DTO;
using Restaurant.DataUnit;
using Restaurant.Entities.Code;

namespace Restaurant.BLL.Services
{
    public class RecipeService : DTOService<RecipeDTO>
    {
        public override bool AddItem(RecipeDTO DTO)
        {
            var item = DTOToEntity(DTO);
            return Unit.RecipesRepository.AddItem(item);
        }

        public override bool AddItems(IEnumerable<RecipeDTO> DTOs)
        {
            List<Recipe> list = new List<Recipe>();
            foreach (RecipeDTO DTO in DTOs)
            {
                list.Add(DTOToEntity(DTO));
            }
            return Unit.RecipesRepository.AddItems(list);
        }

        public override bool ChangeItem(RecipeDTO DTO)
        {
            return Unit.RecipesRepository.ChangeItem(DTOToEntity(DTO));
        }

        public override bool DeleteItem(Guid id)
        {
            return Unit.RecipesRepository.DeleteItem(id);
        }

        public override RecipeDTO GetItem(Guid id)
        {
            Recipe item = Unit.RecipesRepository.GetItem(id);
            return EntityToDTO(item);
        }

        public override bool SaveChanges()
        {
            return Unit.RecipesRepository.SaveChanges();
        }

        public Recipe DTOToEntity(RecipeDTO DTO)
        {
            var item = new Recipe() { TextOfRecipe = DTO.TextOfRecipe };
            return item;
        }

        public RecipeDTO EntityToDTO(Recipe Entity)
        {
            var item = new RecipeDTO() { TextOfRecipe = Entity.TextOfRecipe };
            return item;
        }
    }
}
