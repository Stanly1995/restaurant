﻿using Restaurant.BLL.DTO;
using Restaurant.BLL.Abstractions;

namespace Restaurant.BLL.Interfaces
{
    public interface IStockService: IDTO<StockDTO>
    {
    }
}
