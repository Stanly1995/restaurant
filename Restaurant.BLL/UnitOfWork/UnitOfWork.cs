﻿using Restaurant.BLL.Services;

namespace Restaurant.BLL.UnitOfWork
{
    class UnitOfWork
    {
        public static CounterNameService CounterNameService { get; private set; }
        public static DishService DishService{ get; private set; }
        public static EmployeeService EmployeeService { get; private set; }
        public static IngridientService IngridientService { get; private set; }
        public static PositionService PositionService { get; private set; }
        public static ProductService ProductService { get; private set; }
        public static PurchaseService PurchaseService { get; private set; }
        public static RecipeService RecipeService { get; private set; }
        public static SaleService SaleService { get; private set; }
        public static SellerService SellerService { get; private set; }
        public static StockService StockService { get; private set; }

        static UnitOfWork()
        { 
            CounterNameService = new CounterNameService();
            DishService = new DishService();
            EmployeeService = new EmployeeService();
            IngridientService = new IngridientService();
            PositionService = new PositionService();
            ProductService = new ProductService();
            PurchaseService = new PurchaseService();
            RecipeService = new RecipeService();
            SaleService = new SaleService();
            SellerService = new SellerService();
            StockService = new StockService();
        }
    }
}
