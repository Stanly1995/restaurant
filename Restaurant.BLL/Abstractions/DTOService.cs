﻿using Restaurant.BLL.Infrastructure;
using Restaurant.DataUnit;
using Restaurant.DomainAbstractions.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Restaurant.BLL.Abstractions
{
    public abstract class DTOService<T> : IDTO<T>
        where T : class, IDTOItem
    {
        public abstract bool AddItem(T DTO);
        public abstract bool AddItems(IEnumerable<T> DTOs);
        public abstract bool ChangeItem(T DTO);
        public abstract bool DeleteItem(Guid id);
        public abstract T GetItem(Guid id);
        public abstract bool SaveChanges();
    }
}
