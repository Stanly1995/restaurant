﻿using System;

namespace Restaurant.BLL.Abstractions
{
    public interface IDTOItem
    {
        Guid Id { get; set; }
    }
}
