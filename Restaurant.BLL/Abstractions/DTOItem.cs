﻿using System;
using Restaurant.DomainAbstractions.Entities;

namespace Restaurant.BLL.Abstractions
{
    public class DTOItem : IDTOItem
    {
        public Guid Id { get; set; }
    }
}
