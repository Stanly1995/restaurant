﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Restaurant.BLL.Abstractions
{
    public interface IDTO<T>
    {
        bool AddItem(T DTO);
        bool AddItems(IEnumerable<T> DTOs);
        bool ChangeItem(T DTO);
        bool DeleteItem(Guid id);
        T GetItem(Guid id);
        bool SaveChanges();
    }
}
